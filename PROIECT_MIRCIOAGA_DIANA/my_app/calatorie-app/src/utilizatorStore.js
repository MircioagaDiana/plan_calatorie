import axios from 'axios'
import {EventEmitter} from 'fbemitter'

const SERVER ='http://localhost:8080'

class utilizatorStore{
	constructor(){
		this.content =[]
		this.contentMeteo =[]
		this.contentUser =[]
		this.contentToateCalatoriile =[]
		this.emitter = new EventEmitter()
	}
	async getPrognozaMeteo(oras){
		try{
			
			let response = await axios(`${SERVER}/prognozaMeteo/${oras}`)			
			this.contentMeteo = response.data			
			this.emitter.emit('PROGNOZA_SUCCESS')

		}
		catch(ex){
			console.warn(ex)
			this.emitter.emit('PROGNOZA_ERROR',ex)
		}
	}

	async checkUser(email,parola){
		try{
			
			let response = await axios(`${SERVER}/utilizator/${email}/${parola}`)			
			this.content = response.data			
			this.emitter.emit('CHECK_USER_SUCCESS')

		}
		catch(ex){
			console.warn(ex)
			this.emitter.emit('CHECK_USER_ERROR',ex)
		}
	}

	async addUser(utilizator){
		try{
			
			let response = await axios.post(`${SERVER}/utilizator`,utilizator)								
			this.emitter.emit('CREATE_USER_SUCCESS')

		}
		catch(ex){
			console.warn(ex)
			this.emitter.emit('CREATE_USER_ERROR',ex)
		}
	}

	async getCalatoriiUser(id){
		try{
			
			let response = await axios(`${SERVER}/utilizatori/${id}/calatorii`)			
			this.content = response.data			
			this.emitter.emit('GET_CALATORII_USER_SUCCESS')

		}
		catch(ex){
			console.warn(ex)
			this.emitter.emit('GET_CALATORII_USER_ERROR',ex)
		}
	}

	async getCalatorii(){
		try{
			
			let response = await axios(`${SERVER}/calatorii`)			
			this.contentToateCalatoriile = response.data			
			this.emitter.emit('GET_CALATORII_SUCCESS')

		}
		catch(ex){
			console.warn(ex)
			this.emitter.emit('GET_CALATORII_ERROR',ex)
		}
	}
	async updateCalatorii(id,cid,calatorie){
		try{
			
			let response = await axios.put(`${SERVER}/utilizatori/${id}/calatorii/${cid}`,calatorie)										
			this.getCalatoriiUser(id)
			this.getCalatorii()
			this.emitter.emit('UPDATE_CALATORII_SUCCESS')

		}
		catch(ex){
			console.warn(ex)
			this.emitter.emit('UPDATE_CALATORII_ERROR',ex)
		}
	}

		async createCalatorie(id,calatorie){
		try{		
			let response = await axios.post(`${SERVER}/utilizator/${id}/calatorii`,calatorie)			
			this.getCalatoriiUser(id)
			this.getCalatorii()
			this.emitter.emit('CREATE_CALATORIE_SUCCESS')

		}
		catch(ex){
			console.warn(ex)
			this.emitter.emit('CREATE_CALATORIE_ERROR',ex)
		}
	}
	async deleteCalatorie(id,cid){
		try{
			
			let response = await axios.delete(`${SERVER}/utilizatori/${id}/calatorii/${cid}`)			
			this.getCalatoriiUser(id)
			this.getCalatorii()
			this.emitter.emit('DELETE_CALATORII_SUCCESS')

		}
		catch(ex){
			console.warn(ex)
			this.emitter.emit('DELETE_CALATORII_ERROR',ex)
		}
	}

	async updateNume(id,utilizator){
			try{
				
				let response = await axios.put(`${SERVER}/utilizator/${id}`,utilizator)			
				this.content = response.data			
				this.emitter.emit('UPDATE_USER_SUCCESS')

			}
			catch(ex){
				console.warn(ex)
				this.emitter.emit('UPDATE_USER_ERROR',ex)
			}
		}




}

export default utilizatorStore