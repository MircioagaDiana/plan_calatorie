import React, { Component } from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import AppBar from 'material-ui/AppBar';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import IconButton from 'material-ui/IconButton';
import {TabView,TabPanel} from 'primereact/tabview';
import {DataTable} from 'primereact/datatable';
import {Column} from 'primereact/column';
import PowerSettingsNewIcon from '@material-ui/icons/PowerSettingsNew';
import SvgIcon from '@material-ui/core/SvgIcon';
import { Messages } from 'primereact/messages';
import { Message } from 'primereact/message';
import UtilizatorStore from './utilizatorStore.js'
import {InputText} from 'primereact/inputtext';
import {Button} from 'primereact/button';
import {Dialog} from 'primereact/dialog';
import {Calendar} from 'primereact/calendar';


class Main extends Component {
      constructor(props){
        super(props);
        this.state={
          calatoriileMele:'',      
          toateCalatoriile:'',
          datePrognoza:'',
          email:'',
          password:'',      
          username:'',
          oras:'Bucharest'

        }
        this.id=''
        this.userStore = new UtilizatorStore()
        this.save = this.save.bind(this);
        this.delete = this.delete.bind(this);
        this.onCalatorieSelect = this.onCalatorieSelect.bind(this);
        this.addNew = this.addNew.bind(this);
        this.loadCalatorii = this.loadCalatorii.bind(this);
        this.updateUsername = this.updateUsername.bind(this);
        this.getPrognoza = this.getPrognoza.bind(this)
        this.templateColoana = this.templateColoana.bind(this)
      }

      componentDidMount() {  
        //preluare date tab calatoriile mele
        this.loadCalatorii()
        this.setState({
          email:this.props.email
        })
       
      }


      loadCalatorii(){
        this.userStore.emitter.removeAllListeners()
        this.userStore.getCalatoriiUser(this.props.idUtilizator);
        this.userStore.emitter.addListener('GET_CALATORII_USER_SUCCESS' , () => {        
               this.setState({             
                  calatoriileMele : this.userStore.content
            })                     
           })

        this.userStore.emitter.addListener('GET_CALATORII_USER_ERROR' , (ex) => {
                this.messages.show({life: 7000,severity: 'error', summary: 'Error Message', detail: 'Probleme la preluarea datelor utilizatorului!  => \n' + ex });
            })


        //preluare date tab toate calatoriile
        this.userStore.getCalatorii();
        this.userStore.emitter.addListener('GET_CALATORII_SUCCESS' , () => {        
               this.setState({             
                toateCalatoriile : this.userStore.contentToateCalatoriile                       
             })
          })

        this.userStore.emitter.addListener('GET_CALATORII_ERROR' , (ex) => {    

                this.messages.show({life: 7000,severity: 'error', summary: 'Error Message', detail: 'Probleme la preluarea tuturor calatoriilor !  => \n' + ex });

        })
      }

      //start tab Prognoza meteo
      getPrognoza(){

        this.userStore.emitter.removeAllListeners()
        this.userStore.getPrognozaMeteo(this.state.oras);
        this.userStore.emitter.addListener('PROGNOZA_SUCCESS' , () => {        
             this.setState({             
              datePrognoza : this.userStore.contentMeteo.list                       
        })           
             this.messages.show({ life: 5000, severity: 'success', summary: 'Success Message', detail: 'Orasul '+this.state.oras +' a fost gasit!' });           
       })

        this.userStore.emitter.addListener('PROGNOZA_ERROR' , (ex) => {    

               this.messages.show({life: 7000,severity: 'error', summary: 'Error Message', detail: 'Probleme la preluarea prognozei meteo !  => \n' + ex });

        })
      }

      //templateColoana este folosit pentru afisarea description in tabelul pentru prognoza vremii
      templateColoana(rowData,props){
        let weather = rowData.weather[0];
        return weather[props.field];
      }


      //start tab Calatoriile mele

      getValue(){
        let value =new Date(this.state.calatorie.data_plecare); 
        value.setDate(value.getDate()+1);
        return value;
      }

      save() {

        this.userStore.emitter.removeAllListeners()
        if((this.state.calatorie.oras_plecare.length > 0)&&(this.state.calatorie.oras_destinatie.length > 0)&&(this.state.calatorie.data_plecare.toString().length > 0)&&(this.state.calatorie.numar_persoane.toString().length > 0)&&(this.state.calatorie.tip_transport.length > 0) ){

          if(this.newCalatorie){
            //caz de inserare noua calatorie
            this.userStore.createCalatorie(this.props.idUtilizator,{
              oras_plecare: this.state.calatorie.oras_plecare,
              oras_destinatie: this.state.calatorie.oras_destinatie,
              data_plecare: this.state.calatorie.data_plecare,
              numar_persoane: this.state.calatorie.numar_persoane,
              tip_transport: this.state.calatorie.tip_transport,

            }


            );
            this.userStore.emitter.addListener('CREATE_CALATORIE_SUCCESS' , () => {        
              this.loadCalatorii()
              this.messages.show({ life: 5000, severity: 'success', summary: 'Success Message', detail: 'Calatorie adaugata!' });

            })

            this.userStore.emitter.addListener('CREATE_CALATORIE_ERROR' , (ex) => {    

              this.messages.show({life: 7000,severity: 'error', summary: 'Error Message', detail: 'Probleme la crearea calatoriei !  => \n' + ex });

            })

            //calatoriileMele.push(this.state.calatorie);
          }
          else {
            //caz de update calatorie

            this.userStore.updateCalatorii(this.props.idUtilizator,this.state.calatorie.id,{
              oras_plecare: this.state.calatorie.oras_plecare,
              oras_destinatie: this.state.calatorie.oras_destinatie,
              data_plecare: this.state.calatorie.data_plecare,
              numar_persoane: this.state.calatorie.numar_persoane,
              tip_transport: this.state.calatorie.tip_transport,

            }


            );
            this.userStore.emitter.addListener('UPDATE_CALATORII_SUCCESS' , () => {        
              this.loadCalatorii() 
              this.messages.show({ life: 5000, severity: 'success', summary: 'Success Message', detail: 'Calatorie actualizata!' });

            })

            this.userStore.emitter.addListener('UPDATE_CALATORII_ERROR' , (ex) => {    

              this.messages.show({life: 7000,severity: 'error', summary: 'Error Message', detail: 'Probleme la actualizarea calatoriei !  => \n' + ex });

            })


            //calatoriileMele[this.findSelectedCalatorieIndex()] = this.state.calatorie;



          }

          this.setState({selectedCalatorie:null, calatorie: null, displayDialog:false});
        }
        else{
          this.messages.show({life: 7000,severity: 'error', summary: 'Error Message', detail: 'Toate campurile trebuie sa fie completate!' });
        }
      }

      delete() {
        this.userStore.emitter.removeAllListeners()
        if(!this.newCalatorie){
          this.userStore.deleteCalatorie(this.props.idUtilizator,this.state.calatorie.id);
          this.userStore.emitter.addListener('DELETE_CALATORII_SUCCESS' , () => {        
            this.loadCalatorii()
            this.messages.show({ life: 5000, severity: 'success', summary: 'Success Message', detail: 'Calatorie stearsa!' });

          })

          this.userStore.emitter.addListener('DELETE_CALATORII_ERROR' , (ex) => {    

            this.messages.show({life: 7000,severity: 'error', summary: 'Error Message', detail: 'Probleme la stergerea calatoriei !  => \n' + ex });

          })

          this.setState({            
            selectedCalatorie: null,
            calatorie: null,
            displayDialog: false});
        }
        else{
          this.messages.show({life: 7000,severity: 'error', summary: 'Error Message', detail: 'Nu puteti sterge o calatorie inexistenta!' });
        }
      }

      findSelectedCalatorieIndex() {
        return this.state.calatoriileMele.indexOf(this.state.selectedCalatorie);
      }

      updateProperty(property, value) {
        let calatorie = this.state.calatorie;
        calatorie[property] = value;
        this.setState({calatorie: calatorie});
      }

      onCalatorieSelect(e){
        this.newCalatorie = false;
        this.userStore.emitter.removeAllListeners();
        this.setState({
          displayDialog:true,
          calatorie: Object.assign({}, e.data)
        });
      }

      addNew() {
        this.newCalatorie = true;
        this.userStore.emitter.removeAllListeners();
        this.setState({
          calatorie: {oras_plecare:'', oras_destinatie: '', data_plecare: new Date(), numar_persoane: '',tip_transport:''},
          displayDialog: true
        });
      }

      //end tab Calatoriile mele

      //start tab User

      updateUsername() {       

        this.userStore.emitter.removeAllListeners()
        this.userStore.updateNume(this.props.idUtilizator,{username:this.state.username})
        this.userStore.emitter.addListener('UPDATE_USER_SUCCESS' , () => {
          this.loadCalatorii()                   
          this.props.onUsernameChange(this.state.username)
          this.setState({displayDialogUser:false});
          this.messages.show({ life: 5000, severity: 'success', summary: 'Success Message', detail: 'Username actualizat!' });
        })

        this.userStore.emitter.addListener('UPDATE_USER_ERROR' , (ex) => {    

          this.messages.show({life: 7000,severity: 'error', summary: 'Error Message', detail: 'Probleme la actualizare username !  => \n' + ex });

        })

      }

      //end tab User

      render() {


         let footer = <div className="p-clearfix" style={{width:'100%'}}>
             <Button style={{float:'left'}} label="Add" icon="pi pi-plus" onClick={this.addNew}/>
             </div>;

         let dialogFooter = <div className="ui-dialog-buttonpane p-clearfix">
             <Button label="Delete" icon="pi pi-times" onClick={this.delete}/>
             <Button label="Save" icon="pi pi-check" onClick={this.save}/>
             </div>;

         let dialogFooterUser = <div className="ui-dialog-buttonpane p-clearfix">                
             <Button label="Save" icon="pi pi-check" onClick={this.updateUsername}/>
             </div>;



         var header = <div style={{'textAlign':'left'}}>
             <i className="pi pi-search" style={{margin:'4px 4px 0 0'}}></i>
             <InputText type="search" value={this.state.globalFilter} onInput={(e) => this.setState({globalFilter: e.target.value})} placeholder="Global Search" size="50"/>
             </div>;

         var headerPrognoza = <div style={{'textAlign':'left'}}>
             <i className="pi pi-search" style={{margin:'4px 4px 0 0'}}></i>
             <InputText type="search" value={this.state.globalFilterPrognoza} onInput={(e) => this.setState({globalFilterPrognoza: e.target.value})} placeholder="Global Search" size="50"/>
             </div>;

         return (    
           <div>
             <MuiThemeProvider>
               <Messages ref={(el) => this.messages = el}></Messages>  
               <div >
                 <AppBar
                   title="Vizualizare calatorii " 
                   iconElementLeft={<IconButton><PowerSettingsNewIcon onClick = {() => this.props.handleClick({
                    email : '',
                    username: '',
                    password : ''},
                    this.id = '-2'
                    )}/></IconButton>}             
                    >
                      <h3 style={{color:'white'}}>                     
                      {

                       'Bine ai venit, ' + this.props.username +'!'
                     }
                     </h3>
                 </AppBar>

                 <TabView activeIndex={this.state.activeIndex} onTabChange={(e) => {
                        this.setState({activeIndex: e.index})                  
                      }
                  }>

                  <TabPanel header="Prognoza meteo">
                      <TextField
                        hintText="Introduceti orasul"
                        floatingLabelText="Oras"           
                        value={this.state.oras} 
                        onChange = {(event,newValue) => this.setState({oras:newValue})}           
                        />
                      <RaisedButton label="Cautare" primary={true}  onClick={() => {
                          this.getPrognoza()          
                        }}/>
                      <br/><br/>

                      <DataTable ref={(el) => this.dt = el} value={this.state.datePrognoza} paginator={true} rows={15} globalFilter={this.state.globalFilterPrognoza} emptyMessage="No records found" header={headerPrognoza}>
                        <Column field="dt_txt" header="Ziua si ora" filter={true}/>         
                        <Column field="description" header="Stare" body={this.templateColoana} />
                        <Column field="main.temp" header="Temperatura" filter={true}/>
                        <Column field="main.temp_max" header="Temperatura maxima" filter={true}/>
                        <Column field="main.temp_min" header="Temperatura minima" filter={true} />
                        <Column field="main.humidity" header="Umiditate %" filter={true}/>
                        <Column field="main.pressure" header="Presiune atmosferica" filter={true}/>
                        <Column field="wind.speed" header="Viteza vantului" filter={true}/>
                      </DataTable>
                  </TabPanel>  
                  <TabPanel header="Calatoriile mele">
                    <DataTable value={this.state.calatoriileMele} paginator={true} rows={15}  footer={footer}
                    selectionMode="single" selection={this.state.selectedCalatorie} onSelectionChange={e => this.setState({selectedCalatorie: e.value})}
                    onRowSelect={this.onCalatorieSelect}>
                        <Column field="id" header="id" sortable={true}/>
                        <Column field="oras_plecare" header="Oras plecare" sortable={true} />
                        <Column field="oras_destinatie" header="Destinatie" sortable={true} />
                        <Column field="data_plecare" header="Data plecarii" sortable={true} />
                        <Column field="numar_persoane" header="Numar de persoane" sortable={true} />
                        <Column field="tip_transport" header="Tip transport" sortable={true} />
                    </DataTable>
                    <Dialog visible={this.state.displayDialog} width="300px" header="Detalii" modal={true} footer={dialogFooter} onHide={() => this.setState({displayDialog: false})}>
                        {
                          this.state.calatorie && 

                          <div className="p-grid p-fluid">                               

                          <div className="p-col-4" style={{padding:'.75em'}}><label htmlFor="oras_plecare">Oras plecare</label></div>
                          <div className="p-col-8" style={{padding:'.5em'}}>
                          <InputText id="oras_plecare" onChange={(e) => {this.updateProperty('oras_plecare', e.target.value)}} value={this.state.calatorie.oras_plecare}/>
                          </div>

                          <div className="p-col-4" style={{padding:'.75em'}}><label htmlFor="oras_destinatie">Oras destinatie</label></div>
                          <div className="p-col-8" style={{padding:'.5em'}}>
                          <InputText id="oras_destinatie" onChange={(e) => {this.updateProperty('oras_destinatie', e.target.value)}} value={this.state.calatorie.oras_destinatie}/>
                          </div>

                          <div className="p-col-4" style={{padding:'.75em'}}><label htmlFor="data_plecare">Data plecare</label></div>
                          <div className="p-col-8" style={{padding:'.5em'}}>
                          <Calendar id="data_plecare" utc="true" dataType="date" dateFormat="yy/mm/dd" value={this.getValue()} readOnlyInput={true} onChange={(e) => {this.updateProperty('data_plecare', e.value)}}  showButtonBar={true}></Calendar>                                    
                          </div>
                         
                          <div className="p-col-4" style={{padding:'.75em'}}><label htmlFor="numar_persoane">Numar persoane</label></div>
                          <div className="p-col-8" style={{padding:'.5em'}}>
                          <InputText id="numar_persoane" onChange={(e) => {this.updateProperty('numar_persoane', e.target.value)}} value={this.state.calatorie.numar_persoane}/>
                          </div>

                          <div className="p-col-4" style={{padding:'.75em'}}><label htmlFor="tip_transport">Tip transport</label></div>
                          <div className="p-col-8" style={{padding:'.5em'}}>
                          <InputText id="tip_transport" onChange={(e) => {this.updateProperty('tip_transport', e.target.value)}} value={this.state.calatorie.tip_transport}/>
                          </div>
                          </div>
                        }
                    </Dialog>

                  </TabPanel>
                  <TabPanel header="Toate calatoriile">
                    <DataTable ref={(el) => this.dt = el} value={this.state.toateCalatoriile} paginator={true} rows={15} globalFilter={this.state.globalFilter} emptyMessage="No records found" header={header}>
                      <Column field="utilizator.username" header="Utilizator" filter={true}/>
                      <Column field="oras_plecare" header="Oras plecare" filter={true} />
                      <Column field="oras_destinatie" header="Destinatie" filter={true} />
                      <Column field="data_plecare" header="Data plecarii" filter={true} />
                      <Column field="numar_persoane" header="Numar de persoane" filter={true} />
                      <Column field="tip_transport" header="Tip transport" filter={true} />
                    </DataTable>
                  </TabPanel>  
                  <TabPanel header="Detalii cont">
                    <TextField            
                      floatingLabelText="Email"
                      style={ {margin: '0px auto', display:'block'}}
                      value={this.props.email}           
                    />
                    <TextField
                      hintText="Introduceti username"
                      floatingLabelText="Username"
                      style={ {margin: '0px auto', display:'block'}}
                      value={this.props.username}            
                    />    
                    <br/>
                    <br/>
                    <RaisedButton label="Resetare username" primary={true} style={ {margin: '0px auto', width: '250px',display:'block'}} onClick={() => {
                      this.setState({displayDialogUser:true})
                      this.userStore.emitter.removeAllListeners()
                    }}/>


                    <Dialog visible={this.state.displayDialogUser} width="300px" header="Schimbare username" modal={true} footer={dialogFooterUser} onHide={() => this.setState({displayDialogUser: false})}>
                    {

                      <div className="p-grid p-fluid">                               

                      <div className="p-col-4" style={{padding:'.75em'}}><label htmlFor="usernameUpdate">Username</label></div>
                      <div className="p-col-8" style={{padding:'.5em'}}>
                      <InputText id="usernameUpdate" onChange={(e) => {this.setState({username: e.target.value})} } value={this.state.username}/>
                      </div>

                      </div>
                    }
                    </Dialog>


                  </TabPanel>                   
                </TabView>

              </div>
          </MuiThemeProvider>
        </div>
        );      
      } 
}

export default Main;
