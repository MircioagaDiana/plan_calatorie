import React, { Component } from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import AppBar from 'material-ui/AppBar';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import IconButton from 'material-ui/IconButton';

class Register extends Component {
  constructor(props){
    super(props);
    this.state={
      username:'', 
      email:'',
      password:''      
    }
    this.id=''
  }

  render() {
    return (
      <div>
        <MuiThemeProvider>
          <div >
            <AppBar
              title="Register"
              iconElementLeft={<IconButton></IconButton>}
            />
            <TextField
              hintText="Introduceti adresa de email"
              floatingLabelText="Email"
              style={ {margin: '0px auto', display:'block'}}
              onChange = {(event,newValue) => this.setState({email:newValue})}
            />
            <br/> 
            <TextField
              hintText="Introduceti username - minim 3 caractere"
              floatingLabelText="Username"
              style={ {margin: '0px auto', display:'block'}}
              onChange = {(event,newValue) => this.setState({username:newValue})}
            />
            <br/>
            <TextField
              type="password"
              hintText="Introduceti parola"
              floatingLabelText="Password"
              style={ {margin: '0px auto', display:'block'}}
              onChange = {(event,newValue) => this.setState({password:newValue})}
            />             
          
            <RaisedButton label="Register" primary={true} style={ {margin: '0px auto', width: '150px',display:'block'}} onClick={() => this.props.handleClick({
              username: this.state.username,
              email : this.state.email,              
              parola : this.state.password},
              this.id = '0'

              )}/>
            <br/>
            <RaisedButton label="Back" primary={true} style={ {margin: '0px auto', width: '150px',display:'block'}} onClick={() => this.props.handleClick({
              email : '',
              username: '',
              password : ''},
              this.id = '-2'

              )}/>
          </div>
        </MuiThemeProvider>
      </div>




      );      
    } 
  }

  export default Register;
