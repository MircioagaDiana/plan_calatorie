import React, { Component } from 'react';
import { Button } from 'primereact/button';
import { Panel } from 'primereact/panel';
import 'primereact/resources/themes/nova-light/theme.css';
import 'primereact/resources/primereact.min.css';
import 'primeicons/primeicons.css';
import Login from './login.js'
import Register from './register.js'
import Main from './main.js'
import UtilizatorStore from './utilizatorStore.js'
import { Messages } from 'primereact/messages';
import { Message } from 'primereact/message';


class App extends Component {
  constructor(props){
    super(props);
    this.state={
      utilizator : {},
      loginId :'-2'

    } 
    this.userStore = new UtilizatorStore()
    this.changeUsername = (newUsername) =>{
      this.setState(prevState => {
        let utilizator = Object.assign({}, prevState.utilizator);  
        utilizator.username = newUsername;                                    
        return { utilizator };                                
      })
    }
    this.handleClick = (user,id) => {   
      this.messages.clear();    
      console.warn(id)
      if(id === '1'){
        //caz incercare login  

        this.userStore.checkUser(user.email,user.password)
        this.userStore.emitter.addListener('CHECK_USER_SUCCESS' , () => {        
         this.setState({             
          utilizator : this.userStore.content,
          loginId :id               
        })
         this.messages.show({ life: 5000, severity: 'success', summary: 'Success Message', detail: 'Conectat!' });
         this.userStore.emitter.removeAllListeners();
         
       })

        this.userStore.emitter.addListener('CHECK_USER_ERROR' , (ex) => {    

          this.messages.show({severity: 'error', summary: 'Error Message', detail: 'User-ul nu exista!  => ' + ex });
          this.userStore.emitter.removeAllListeners();
        })



      }
      else{
        if(id==='0'){
          ///caz adaugare utilizator nou
          this.userStore.addUser(user)
          this.userStore.emitter.addListener('CREATE_USER_SUCCESS' , () => {        
           this.setState({             
            utilizator : user,
            loginId :id               
          })
           this.messages.show(
            { severity: 'success', summary: 'Success Message', detail: 'Utilizatorul a fost creat!' },
            { life: 5000, severity: 'success', summary: 'Success Message', detail: 'Conectat!' }
            );
           this.userStore.emitter.removeAllListeners();
         })

          this.userStore.emitter.addListener('CREATE_USER_ERROR' , (ex) => {    

            this.messages.show({life: 7000,severity: 'error', summary: 'Error Message', detail: 'Probleme la crearea utilizatorului!  =>  Verificati ca adresa de email sa fie corecta, iar username-ul si parola sa aiba intre 3 si 20 de caractere!  => \n' + ex });
            this.userStore.emitter.removeAllListeners();
          })


        }
        else{
          //caz afisare ecran register
          this.setState({loginId:id})
          
        }
      }

    }

  }

 render() {

    if(this.state.loginId === '-2'){ 
      return (
        <div>    
          {
            <Messages ref={(el) => this.messages = el}></Messages>  
          }
          <Login handleClick= { this.handleClick }/>
        </div>
        );
      }
    else {  
      if(this.state.loginId === '-1'){   
        return(    
          <div>
            <Messages ref={(el) => this.messages = el}></Messages>
            <Register handleClick= { this.handleClick }/>
          </div>      
          );
        }
      else{
        return ( 
          <div>    
             <Messages ref={(el) => this.messages = el}></Messages>    
            {         

              <Main username={this.state.utilizator.username} idUtilizator={this.state.utilizator.id} email={this.state.utilizator.email} handleClick= { this.handleClick } onUsernameChange={this.changeUsername}/>
            }
          </div> 
          );
        }
      }


    } 
  }

  export default App;
