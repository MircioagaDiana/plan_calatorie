import React, { Component } from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import AppBar from 'material-ui/AppBar';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import IconButton from 'material-ui/IconButton';


class Login extends Component {
constructor(props){
  super(props);
  this.state={
  email:'',
  password:''  
  }
  this.id=''
}


  render() {
    return (
      <div>
          <MuiThemeProvider>
            <div >
              <AppBar
                 title="Login"
                 iconElementLeft={<IconButton></IconButton>}
               />          
               <TextField
                 hintText="Introduceti adresa de email"
                 floatingLabelText="Email"
                 style={ {margin: '0px auto', display:'block'}}
                 onChange = {(event,newValue) => this.setState({email:newValue})}
                 />
               <br/>
                 <TextField
                   type="password"
                   hintText="Introduceti parola"
                   floatingLabelText="Password"
                   style={ {margin: '0px auto', display:'block'}}
                   onChange = {(event,newValue) => this.setState({password:newValue})}
                   />
                 <br/>
                 <RaisedButton label="Login" primary={true} style={ {margin: '0px auto', width: '150px',display:'block'}} onClick={() => this.props.handleClick({
                  email : this.state.email,
                  password : this.state.password 
                },
                  this.id = '1'

                 )}/>
                 <br/><br/>
                 <label style={ {margin: '0px auto', width: '250px' ,display:'block'}}>Nu aveti cont? Inregistrati-va!</label>
                 <br/>
                
                 <RaisedButton label="Register" primary={true} style={ {margin: '0px auto', width: '150px',display:'block'}} onClick={() => this.props.handleClick({
                  email : '',
                  password : ''},
                  this.id = '-1'

                 )}/>
           </div>
         </MuiThemeProvider>
      </div>




    );      
  } 
}

export default Login;
