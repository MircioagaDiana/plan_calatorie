const express = require('express')
const bodyParser = require('body-parser')
const Sequelize = require('sequelize')
const axios = require('axios')
require('sequelize-isunique-validator')(Sequelize);

// Initializarea conexiunii catre baza de date mysql
const sequelize = new Sequelize('plan_calatorie','diana','welcome123', {
	dialect:'mysql',
	define:{
		timestamps:false
	}

})

// Definirea tabelei utilizator
const Utilizator = sequelize.define('utilizator',{
	username :{
		allowNull : false,
		type: Sequelize.STRING,
		validate : {
			len : {
				args:[3,20],
				msg : 'Numele de utilizator trebuie sa aiba intre 3 si 20 de caractere!'

			}
		}
	},
	email : {
		allowNull: false,
		type: Sequelize.STRING,
		validate : {
			isEmail : {
				msg: 'Adresa de email nu este valida!'
			}},
		unique: true
		
	},
	parola : {
		allowNull: false,
		type: Sequelize.STRING,
		validate : {
			len : {
				args:[3,20],
				msg : 'Parola trebuie sa aiba intre 3 si 20 de caractere!'

			}
		}
	}


},{
	underscored: true
})

//definirea tabelei calatorie
const Calatorie = sequelize.define('calatorie',{
	oras_plecare :{
		allowNull : false,
		type: Sequelize.STRING,
		validate : {
			len : {
				args:[1,100],
				msg : 'Orasul trebuie sa aiba intre 1 si 100 de caractere!'

			}
		}
	},
	oras_destinatie :{
		allowNull : false,
		type: Sequelize.STRING,
		validate : {
			len : {
				args:[1,100],
				msg : 'Orasul trebuie sa aiba intre 1 si 100 de caractere!'

			}
		}
	},
	data_plecare :{
		allowNull : false,
		type: Sequelize.DATEONLY		
	},
	numar_persoane :{
		allowNull : false,
		type: Sequelize.INTEGER		
	},
	tip_transport :{
		allowNull : false,
		type: Sequelize.STRING		
	}

})

//definirea relatiei one-to-many( un utilizator poate avea mai multe calatorii). Totodata, cu ajutorul onDelete setat cu valoarea 'cascade' ne asiguram ca vom sterge toate calatoriile unui utilizator - odata cu stergerea acestuia
Utilizator.hasMany(Calatorie, { onDelete: 'cascade' })
Calatorie.belongsTo(Utilizator)

const app = express()
app.use(bodyParser.json())
app.use(express.static('my_app/calatorie-app/build'))

//START SECTIUNEA GET

//START OPENWEATHERMAP INTEGRATION
app.get('/prognozaMeteo/:oras', async(req,res) => {
	try{
		 let appKey='b5ad2b2bc4bd1fd871a6b790880e30a3'
		 const weatherURL = `http://api.openweathermap.org/data/2.5/forecast?q=${req.params.oras}&units=metric&lang=ro&APPID=${appKey}`		 
		axios.get(weatherURL)
		  .then(response => {			  	 
		    res.status(200).json(response.data)
		  })
		  .catch(error => {
		    console.log(error);
		  });

		
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message:'Eroare de server!'})
	}
})

//definirea metodei get pentru crearea tabelelor
app.get('/create_tables', async (req,res) => {
	try{
		await sequelize.sync({force : true}).then(() =>Utilizator.bulkCreate([{
			username :'diana',
			email:'dianamircioaga@yahoo.com',
			parola:'welcome123'
		},{
			username :'alexandra',
			email:'alexandra@yahoo.com',
			parola:'welcome123'
		}])).then(() => Calatorie.bulkCreate([{
			oras_plecare:'Bucuresti',
			oras_destinatie:'Brasov',
			data_plecare:'2019-11-28',
			numar_persoane:2,
			tip_transport:'Masina',
			utilizatorId:1
		},
		{
			oras_plecare:'Bucuresti',
			oras_destinatie:'Sibiu',
			data_plecare:'2019-11-30',
			numar_persoane:3,
			tip_transport:'Masina',
			utilizatorId:1
		},
		{
			oras_plecare:'Bucuresti',
			oras_destinatie:'Predeal',
			data_plecare:'2019-11-29',
			numar_persoane:4,
			tip_transport:'Masina',
			utilizatorId:2
		}
		]))
		res.status(201).json({message: 'Tabelele au fost create!'})
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message:'Eroare de server!'})
	}
})



//definirea metodei get pentru preluarea verificarea unui utilizator
app.get('/utilizator/:email/:parola', async(req,res) => {
	try{
		let utilizatori = await Utilizator.findAll({where :{email : req.params.email, parola : req.params.parola} })	
		let utilizator= utilizatori.shift()
		if(utilizator){			
			res.status(200).json(utilizator)
		}
		else{
			res.status(404).json({message:'Utilizator inexistent!'})
		}
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message:'Eroare de server!'})
	}
})


//definirea metodei get pentru preluarea tuturor calatoriilor
app.get('/calatorii', async(req,res) => {
	try{
		let calatorie = await Calatorie.findAll({include :[Utilizator]})
		res.status(200).json(calatorie)
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message:'Eroare de server!'})
	}
})


//definirea metodei get pentru preluarea tuturor calatoriilor unui utilizator dupa un id dat
app.get('/utilizatori/:id/calatorii', async(req,res) => {
	try{
		let utilizatori = await Utilizator.findByPk(req.params.id)
		if(utilizatori){
			let calatorie = await utilizatori.getCalatories()
			if(calatorie){
				res.status(200).json(calatorie)
			}
			else{
				res.status(404).json({message:'Nu exista calatorii!'})
			}
			
		}
		else{
			res.status(404).json({message:'Utilizator inexistent!'})
		}
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message:'Eroare de server!'})
	}
})

//END SECTIUNEA GET


//START SECTIUNEA POST
//definirea metodei post pentru adaugarea unui utilizator 
app.post('/utilizator', async(req,res) => {
	try{
		
		if(req.query.bulk && req.query.bulk=='on'){
			await Utilizator.bulkCreate(req.body)
			res.status(201).json({message:'Utilizatorii au fost creati!'})
		}
		else{			
			await Utilizator.create(req.body)
			res.status(201).json({message:'Utilizatorul a fost creat!'})
		}
		
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message:'Eroare de server!'})
	}
})

//definirea metodei post pentru adaugarea unei calatorii aferente unui utilizator 
app.post('/utilizator/:id/calatorii', async(req,res) => {
	try{
		let utilizator = await Utilizator.findByPk(req.params.id)
		if(utilizator){
			let calatorii = req.body
			calatorii.utilizatorId = utilizator.id
			await Calatorie.create(calatorii)
			res.status(201).json({message:'Calatoria a fost adaugata!'})
		}
		else {
			res.status(404).json({message:'Utilizator inexistent!'})
		}	
		
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message:'Eroare de server!'})
	}
})



//END SECTIUNEA POST

//START SECTIUNEA PUT

//definirea metodei put pentru actualizarea unui utilizator dupa un id dat
app.put('/utilizator/:id', async(req,res) => {
	try{
		let utilizatori = await Utilizator.findByPk(req.params.id)
		if(utilizatori){
			await utilizatori.update(req.body)
		res.status(202).json({message:'Utilizator actualizat!'})
		}
		else{
			res.status(404).json({message:'Utilizator inexistent!'})
		}
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message:'Eroare de server!'})
	}
})

//definirea metodei put pentru actualizarea unei calatorii aferente unui utilizator
app.put('/utilizatori/:id/calatorii/:cid', async(req,res) => {
	try{
	
		let utilizatori = await Utilizator.findByPk(req.params.id)
		if(utilizatori){
			let calatories = await utilizatori.getCalatories({where :{id : req.params.cid}})
			let calatorie = calatories.shift()
			if(calatorie){
				await calatorie.update(req.body)
				res.status(202).json({message:'Calatorie actualizata!'})
			}
			else{
				res.status(404).json({message:'Calatoria nu exista!'})
			}
		
		}
		else{
			res.status(404).json({message:'Utilizator inexistent!'})
		}
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message:'Eroare de server!'})
	}
})



//END SECTIUNEA PUT



//START SECTIUNEA DELETE

//definirea metodei delete pentru stergerea unei calatorii aferente unui utilizator
app.delete('/utilizatori/:id/calatorii/:cid', async(req,res) => {
	try{
		let utilizatori = await Utilizator.findByPk(req.params.id)
		if(utilizatori){
			let calatories = await utilizatori.getCalatories({where :{id : req.params.cid}})
			let calatorie = calatories.shift()
			if(calatorie){
				await calatorie.destroy()
				res.status(202).json({message:'Calatorie stearsa!'})
			}
			else{
				res.status(404).json({message:'Calatoria nu exista!'})
			}
		
		}
		else{
			res.status(404).json({message:'Utilizator inexistent!'})
		}
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message:'Eroare de server!'})
	}
})




//END SECTIUNEA DELETE








app.listen(8080)
