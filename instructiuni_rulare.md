## Instructiuni rulare

---

Pentru a putea rula aplicația, în urma descărcării acesteia din repository-ul Bitbucket, este necesară efectuarea următorilor pași:

1. Crearea bazei de date locale în MySQL: CREATE DATABASE plan_calatorie;
2. Crearea user-ului de connectare:  GRANT ALL PRIVILEGES ON \*.* TO 'diana' IDENTIFIED BY 'welcome123';
3. Executarea fișierului server.js: node server.js
4. Crearea tabelelor bazei de date și generarea de date de test. Aceasta operațiune se realizează apelând dintr-un browser următorul URL: localhost:8080/create_tables
5. Accesarea aplicației de front-end prin rularea în browser a următorului URL: localhost:8080
6. Utilizarea aplicației front-end se bazează pe existența unui utilizator. Astfel, se poate folosi email-ul dianamircioaga@yahoo.com și parola welcome123, sau se poate realiza un nou utilizator prin folosirea funcționalității de REGISTER


---

## Serviciu RESTful

---

Serviciul RESTful pune la dispoziție următoarele posibilități de apel pentru interacțiunea cu baza de date sau cu serviciul extern:

1. Metoda GET: Crearea tabelelor bazei de date și generarea de date de test. Această operațiune se realizează apelând dintr-un browser următorul URL: localhost:8080/create_tables
2. Metoda GET: Preluarea datelor de prognoza meteo din serviciul extern. Această operațiune se realizează apelând dintr-un browser următorul URL: localhost:8080/prognozaMeteo/Bucharest. Această metodă primește ca parametru numele orașului dorit, în acest exemplu: Bucharest.
3. Metoda GET: Verificarea existenței unui utilizator, folosit pentru funcționalitatea de login. Această operațiune se realizează apelând dintr-un browser următorul URL: localhost:8080/utilizator/dianamircioaga@yahoo.com/welcome123. Această metodă primește ca parametrii adresa de email și parola unui utilizator.
4. Metoda GET: Preluarea tuturor planurilor de călătorie existente în aplicație. Această operațiune se realizează apelând dintr-un browser următorul URL: localhost:8080/calatorii
5. Metoda GET: Preluarea planurilor de călătorie aferente unui utilizator. Această operațiune se realizează apelând dintr-un browser următorul URL: localhost:8080/utilizatori/1/calatorii.  Această metodă primește ca parametru id-ul utilizatorului, în acest exemplu: 1
6. Metoda POST: Adăugarea unui utilizator. Această operațiune se realizează apelând dintr-un utilitar, cum ar fi POSTMAN, cu opțiunea POST, următorul URL: localhost:8080/utilizator. În cadrul body-ului de apel vor trebui completate câmpurile username,email și parola.
7. Metoda POST: Adăugarea unui plan de călătorie aferent unui utilizator. Această operațiune se realizează apelând dintr-un utilitar, cum ar fi POSTMAN, cu opțiunea POST, următorul URL: localhost:8080/utilizator/1/calatorii. Această metodă primește ca parametru id-ul utilizatorului, în acest exemplu: 1. În cadrul body-ului de apel vor trebui completate câmpurile oras_plecare, oras_destinatie, data_plecare, numar_persoane și tip_transport.
8. Metoda PUT: Actualizarea unui utilizator după un id dat. Această operațiune se realizează apelând dintr-un utilitar, cum ar fi POSTMAN, cu opțiunea PUT, următorul URL: localhost:8080/utilizator/1. Această metodă primește ca parametru id-ul utilizatorului, în acest exemplu: 1. În cadrul body-ului de apel vor trebui completate câmpurile username,email și parola.
9. Metoda PUT: Actualizarea unui plan de călătorie aferent unui utilizator. Această operațiune se realizează apelând dintr-un utilitar, cum ar fi POSTMAN, cu opțiunea PUT, următorul URL: localhost:8080/utilizator/1/calatorii/2. Această metodă primește ca parametru id-ul utilizatorului, în acest exemplu: 1, dar și id-ul călătoriei, în acest exemplu: 2. În cadrul body-ului de apel vor trebui completate câmpurile oras_plecare, oras_destinatie, data_plecare, numar_persoane și tip_transport.
10. Metoda DELETE: Ștergerea unei călătorii aferente unui utilizator. Această operațiune se realizează apelând dintr-un utilitar, cum ar fi POSTMAN, cu opțiunea DELETE, următorul URL: localhost:8080/utilizator/1/calatorii/2. Această metodă primește ca parametru id-ul utilizatorului, în acest exemplu: 1, dar și id-ul călătoriei, în acest exemplu: 2. 

---
