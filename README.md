## Plan de calatorie integrat cu Open Weather Map

---

## Obiectiv

Realizarea unei aplicații web prin care utilizatorii pot sa isi realizeze propriile planuri de calatorie.

---

## Descriere

Aplicația va permite crearea unui cont prin care utilizatorul poate să realizeze o planificare de călătorie sau sa vizualizeze planurile de calatorie ale sale sau ale altor utilizatori.
Realizarea unei planificări de călătorie implică completarea orașului de plecare, destinației, data plecării, numărul de persoane și tipul de transport. 
Aplicația oferă și posibilitatea de vizualizare a prognozei meteo pentru următoarele 5 zile. Aceasta funcționalitate este posibilă prin integrarea cu un API gratuit oferit de către Open Weather Map pentru oferirea de prognoză meteorologică. În cazul utilizării unei versiuni ce nu este gratuită, restricția de 5 zile poate fi eliminată.

Aplicația web urmărește o arhitectură de tip Single Page Application. Back-end-ul aplicației are o interfață REST ce este realizată în node.js, expunând datele stocate într-o bază de date locală(MySQL), iar partea de front-end este realizată cu ajutorul framework-ului bazat pe componente React.js.

---

## Functionalitati

**Modulul Utilizator **

- Creare cont utilizator 
- Modificarea numelui utilizatorului

**Modulul de Partajare **

- Crearea unui plan de calatorie presupune completarea următoarelor câmpuri:

1. Oras plecare
2. Destinatie
3. Data plecare
4. Numar de persoane
5. Tip de transport: avion, masina, etc.

- Modificarea intrărilor specifice utilizatorului
- Listarea tuturor planurilor de calatorie
- Ștergerea unui plan de calatorie

**Modulul de Căutare **

- Modulul de căutare va trebui să permită utilizatorului introducerea unor cuvinte cheie, după care vor fi afișate rezultatele, sub formă de tabel.
- Modulul va trebui să returneze rezultate relevante în funcție de locație, mijlocul de transport folosit sau destinație
- Modulul de căutare va trebui de asemenea să permită introducerea numelui unui oraș si sa afișeze prognoza meteorologică


---